# README #

Working example of openconnect VPN configuration using overplay service.
Tested on Ubuntu 16.04.

### How do I get set up? ###

* Requires openvpn client software and overplay account.
* bin/op starts the connection
* Place authentication in /etc/openvpn/auth.txt. See example-auth.txt and don't forget to append "at-overplay" to all usernames.
* connections holds the configuration.